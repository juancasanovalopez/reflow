/*
 * Copyright (c) 2009 Andrew Smallbone <andrew@rocketnumbernine.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "spi.h"


void setup_spi(uint8_t mode, int dord, int interrupt, uint8_t clock)
{
  // specify pin directions for SPI pins on port B

    DDRB |= (1<<SPI_MOSI_PIN); // output
    DDRB &= ~(1<<SPI_MISO_PIN); // input
    DDRB |= (1<<SPI_SCK_PIN);// output
    DDRB |= (1<<SPI_SS_PIN);// output

  SPCR = ((interrupt ? 1 : 0)<<SPIE) // interrupt enabled
    | (1<<SPE) // enable SPI
    | (dord<<DORD) // LSB or MSB
    | (((clock != SPI_SLAVE) ? 1 : 0) <<MSTR) // Slave or Master
    | (((mode & 0x02) == 2) << CPOL) // clock timing mode CPOL
    | (((mode & 0x01)) << CPHA) // clock timing mode CPHA
    | (((clock & 0x02) == 2) << SPR1) // cpu clock divisor SPR1
    | ((clock & 0x01) << SPR0); // cpu clock divisor SPR0
  SPSR = (((clock & 0x04) == 4) << SPI2X); // clock divisor SPI2X
}


	//		t0		<->	ATmega[14]	<->	PORTB:0	<-> PB0	<->	PINB0	<-	1
	//		t1		<->	ATmega[23]	<->	PORTC:0	<->	PC0	<->	PINC0 	<-	1
	//		t2		<->	ATmega[24]	<->	PORTC:1	<->	PC1	<->	PINC1	<-	1
	//		t3		<->	ATmega[17]	<->	PORTC:2	<->	PC2	<->	PINC2	<-	1

void termocouple_read(uint8_t CS, uint8_t *h_Byte, uint8_t *l_Byte)
{
	switch (CS) //cs3-PC2
	{
		case 0:
			PORTB &= ~(_BV(0)); // active-low
			DDRB |= _BV(0);
			//_delay_us(3);
			*h_Byte = send_spi(0xFF);
			//_delay_us(2);
			*l_Byte = send_spi(0xFF);
			PORTB |= _BV(0);
		break;
	    case 1:
	    	PORTC &= ~(_BV(0)); // active-low
	    	DDRC |= _BV(0);
	    	//_delay_us(3);
	    	*h_Byte = send_spi(0xFF);
	    	//_delay_us(2);
	    	*l_Byte = send_spi(0xFF);
	    	PORTC |= _BV(0);
	     break;
	     case 2:
	    	 PORTC &= ~(_BV(1)); // active-low
	    	 DDRC |= _BV(1);
	    	 //_delay_us(3);
	    	 *h_Byte = send_spi(0xFF);
	    	 //_delay_us(2);
	    	 *l_Byte = send_spi(0xFF);
	    	 PORTC |= _BV(1);
	     break;
	     case 3:
	     	  PORTC &= ~(_BV(2)); // active-low
	     	  DDRC |= _BV(2);
	     	  //_delay_us(3);
	     	  *h_Byte = send_spi(0xFF);
	     	  //_delay_us(2);
	     	  *l_Byte = send_spi(0xFF);
	     	  PORTC |= _BV(2);
	     break;
	    //default:
	      // if nothing else matches, do the default
	      // default is optional
	  }
}

uint8_t termopar_abierto(uint8_t *l_Byte)
{
	uint8_t open = 0;
	//detectar termopar abierto si bit D2 = 1
	if((*l_Byte & 0x04)==0x04)
	{
		open = 1;
	}
	return open;
}

void disable_spi()
{
  SPCR = 0;
}

uint8_t send_spi(uint8_t out)
{
  SPDR = out;
  while (!(SPSR & (1<<SPIF)));
  return SPDR;
}

uint8_t received_from_spi(uint8_t data)
{
  SPDR = data;
  return SPDR;
}
