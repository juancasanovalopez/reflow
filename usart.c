#include <avr/io.h>
#include <stdio.h>
#include "usart.h"

/**************************************
 FUNCION:*
 ENTRADA:*
 SALIDA:*
 **************************************/
void setup_USART( unsigned int ubrr)
{
	/*Set baud rate */
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	//Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set frame format: 8data, 2stop bit */
	UCSR0C = (1<<USBS0)|(3<<UCSZ00);
}

/**************************************
 FUNCION:*
 ENTRADA:*
 SALIDA:*
 **************************************/
void USART_Sendbyte( unsigned char data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) );
	/* Put data into buffer, sends the data */
	UDR0 = data;
}

/**************************************
 FUNCION:*
 ENTRADA:*
 SALIDA:*
 **************************************/
void USART_Send_string(const char *str)
{
	while (*str)
    USART_Sendbyte(*str++);
}

/**************************************
 FUNCION:*
 ENTRADA:*
 SALIDA:*
 **************************************/
void USART_Send_hex(uint8_t h )
{
	char str[10];
	sprintf(str,"%x",h);
	USART_Send_string(str);
}

/**************************************
 FUNCION:*
 ENTRADA:*
 SALIDA:*
 **************************************/
void USART_Send_int(unsigned int d )
{
	char str[10];
	sprintf(str,"%u",d);
	USART_Send_string(str);
}

/**************************************
 FUNCION:*
 ENTRADA:*
 SALIDA:*
 **************************************/
unsigned char USART_Receive( void )
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) );
	/* Get and return received data from buffer */
	return UDR0;
}








