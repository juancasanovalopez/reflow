

//SSR - PINOUT
//
//	P4 -> RHOT0	<->	ATmega[12]	<->	PORTD:6	<-> PD6	<->	PIND6 <-> RESISTENCIA_REFLUJO
//	P3 -> MOT0	<->	ATmega[11]	<->	PORTD:5	<->	PD5	<->	PIND5
//	P2 -> MOT1	<->	ATmega[5]	<->	PORTD:3	<->	PD3	<->	PIND3 <-> VENTILADOR_REFLUJO
//	P1 -> RHOT1	<->	ATmega[15]	<->	PORTB:1	<->	PB1	<->	PINB1 <-> RESISTENCIA_SUPERIOR

/*
 * Copyright (c) 2009 Andrew Smallbone <andrew@rocketnumbernine.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "ssr.h"

void setup_ssr()
{
	DDRD |= (1<<RESISTENCIA_REFLUJO);
	DDRD |= (1<<VENTILADOR_REFLUJO);
	DDRB |= (1<<RESISTENCIA_SUPERIOR);
	DDRD |= (1<<VENTILADOR_INFERIOR);
}

void encender(uint8_t ch)
{
	switch(ch){
  case 1:
    PORTD |= (1<<RESISTENCIA_REFLUJO);
	break;
  case 2:
    PORTB |= (1<<RESISTENCIA_SUPERIOR);
	break;
  case 3:
    PORTD |= (1<<VENTILADOR_REFLUJO);
	break;
  case 4:
    PORTD |= (1<<VENTILADOR_INFERIOR);
  	break;}
}

void apagar(uint8_t ch)
{
	switch(ch){
	case 1:
		PORTD &= ~(1<<RESISTENCIA_REFLUJO);
		break;
	  case 2:
	    PORTB &= ~(1<<RESISTENCIA_SUPERIOR);
		break;
	  case 3:
	    PORTD &= ~(1<<VENTILADOR_REFLUJO);
		break;
	  case 4:
	    PORTD &= ~(1<<VENTILADOR_INFERIOR);
	  	break;
	  }
}

void todosArriba()
{
	PORTD |= (1<<RESISTENCIA_REFLUJO);
	PORTD |= (1<<VENTILADOR_REFLUJO);
	PORTB |= (1<<RESISTENCIA_SUPERIOR);
	PORTD |= (1<<VENTILADOR_INFERIOR);
}

void todosAbajo()
{
	PORTD &= ~(1<<RESISTENCIA_REFLUJO);
	PORTD &= ~(1<<VENTILADOR_REFLUJO);
	PORTB &= ~(1<<RESISTENCIA_SUPERIOR);
	PORTD &= ~(1<<VENTILADOR_INFERIOR);
}

void soplar(uint8_t rendimiento)
{
	uint8_t i;
	PORTD |= (1<<VENTILADOR_REFLUJO);
	for (i=0;i<rendimiento;i++)
	{
		_delay_ms(5);
	}
	PORTD &= ~(1<<VENTILADOR_REFLUJO);
	for (i=0;i<(100-rendimiento);i++)
	{
		_delay_ms(5);
	}
}
