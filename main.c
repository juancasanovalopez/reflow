/*
 * 	Programa Principal
 * 	Horno de Refusión
 */

/*				  ______
			PC6	-|º		|-	PC5
			PD0	-|		|-	PC4
			PD1	-|	A	|-	PC3
			PD2	-|	T	|-	PC2
			PD3	-|	m	|-	PC1
			PD4	-|	e	|-	PC0
			VCC	-|	g	|-	GND
			GND	-|	a	|-	AREF
			PB6	-|	3	|-	AVCC
			PB7	-|	2	|-	PB5
			PD5	-|	8	|-	PB4
			PD6	-|	P	|-	PB3
			PD7	-|		|-	PB2
			PB0	-|______|-	PB1
*/


//#include "intspi.h"

// Librerias estandar AVR
#include <util/delay.h>
#include <avr/io.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>


//Libreria Propias PFC reflow
#include "usart.h"
#include "spi.h"
#include "i2c/lcdpcf8574.h"
#include "pid.h"
#include "ssr.h"
#include "timer.h"
#include "millis.h"

#define TRUE 1
#define FALSE 0

#define RESISTENCIA_RE	1
#define RESISTENCIA_SU	2
#define VENTILADOR_RE	3
#define VENTILADOR_INF	4

#define MAX 280
#define MIN 20

#define MAX_SAMPLES 360
#define PREHEAT 75
#define HEAT 193
#define LIQUID 211
#define LIQUIDPEAKUP 226
#define LIQUIDPEAKDWN 241

#define DIRECT 0
#define REVERSE 1


// DECLARACIONES FUNCIONES PRINCIPALES
void mensajeLCD();
void mensajeSerial();
void mantenerInterior(uint16_t *tempMedia,uint16_t tempDeseada);
void leerTermopares(uint16_t tablaTemperaturas[3]);
uint16_t tempMedia(uint16_t tablaTemp[3]);

unsigned long lastTime;
double Input, Output, Setpoint;
double ITerm, lastInput;
double kp, ki, kd;
unsigned int SampleTime = 1000; // Tiempo de muestreo 1 segundo.
double outMin, outMax;
unsigned int controllerDirection = DIRECT;
double num_muestras;


/* Establecemos los valores de las constantes para la sintonización.
Debido a que ahora sabemos que el tiempo entre muestras es constante,
no hace falta multiplicar una y otra vez por el cambio de tiempo;
podemos ajustar las constantes Ki y Kd, obteniendose un resultado
matemático equivalente pero más eficiente que en la primera versión de la función. */
void SetTunings(double Kp, double Ki, double Kd)
{
	if (Kp<0 || Ki<0|| Kd<0) return;
	double SampleTimeInSec = ((double)SampleTime)/1000;
	kp = Kp;
	ki = Ki * SampleTimeInSec;
	kd = Kd / SampleTimeInSec;
	if(controllerDirection ==REVERSE)
	{
		kp = (0 - kp);
		ki = (0 - ki);
		kd = (0 - kd);
	}
}

/*void SetSampleTime(int NewSampleTime)
{
	if (NewSampleTime > 0)
	{
		doubleratio =(double)NewSampleTime/(double)SampleTime;
		ki *= ratio;
		kd /= ratio;
		SampleTime = (unsigned long)NewSampleTime;
	}
}*/

void SetOutputLimits(double Min, double Max)
{
	if(Min > Max) return;
	outMin = Min;
	outMax = Max;
	if(Output > outMax) Output = outMax;
	else if(Output < outMin) Output = outMin;
	if(ITerm> outMax) ITerm= outMax;
	else if(ITerm< outMin) ITerm= outMin;
}

void Initialize()
{
	lastInput = Input;
	ITerm = Output;
	if(ITerm> outMax) ITerm= outMax;
	else if(ITerm< outMin) ITerm= outMin;
}

void SetControllerDirection(int Direction)
{
	controllerDirection = Direction;
}

//Programa Principal
int main()
{
	setup_USART(MYUBRR);
	setup_spi(SPI_MODE_1, SPI_MSB, SPI_NO_INTERRUPT, SPI_MSTR_CLK128);
	lcd_init(LCD_DISP_ON);
		uint8_t led = 0;
		lcd_led(led); //set led
	setup_ssr();
	sei();
	millis_init();
	//__enable_interrupt();

	//! TODO oven_test(); comprobar termopares tipo K y reflejar en display

	///////////////////////////////
	SetTunings(1, 0, 0);
	SetOutputLimits(MIN,MAX);
	Initialize();

	///////////////////////////////


	while (1)
	{
		lcd_home();
		uint16_t TablaTemperaturas[4];
		leerTermopares(&TablaTemperaturas);
		mensajeSerial(&TablaTemperaturas);
		mensajeLCD(&TablaTemperaturas);
		uint16_t AVGtemp = tempMedia(&TablaTemperaturas);
		uint16_t SalidaPID;
		uint16_t NumMuestras;

		//////////////////////////////////////

		unsigned long now = millis();
		int timeChange = (now - lastTime);
		if(timeChange>=SampleTime)
		{
			lcd_gotoxy(12, 2);
							lcd_puts("Dentro:");

					num_muestras++;
					if (num_muestras < MAX_SAMPLES)
					{
						if (num_muestras < PREHEAT)
						{
							Setpoint = (95/100) * num_muestras + 25;
						}
						else if (num_muestras < HEAT)
						{
							Setpoint = (66/100) * num_muestras + 100;
						}
						else if (num_muestras < LIQUID)
						{
							Setpoint = 3 * num_muestras + 199;
						}
						else if (num_muestras < LIQUIDPEAKUP)
						{
							Setpoint = (33/100) * num_muestras + 253;
						}
						else if (num_muestras < LIQUIDPEAKDWN)
						{
							Setpoint = -(33/100) * num_muestras + 258;
						}
						else
						{
							Setpoint = -6 * num_muestras + 253;
						}
					}
			// Calculamos todos los errores.
			double error = Setpoint - Input;
			ITerm+= (ki * error);
			if(ITerm> outMax) ITerm= outMax;
			else if(ITerm< outMin) ITerm= outMin;
			double dInput = (Input - lastInput);
			// Calculamos la función de salida del PID.
			Output = kp * error + ITerm- kd * dInput;
			if(Output > outMax) Output = outMax;
			else if(Output < outMin) Output = outMin;
			// Guardamos el valor de algunas variables para el próximo recálculo.
			lastInput = Input;
			lastTime = now;
		}
		//////////////////////////////////////


		double potencia;
		potencia = Output/(MAX-MIN);
		potencia *= 100;

		uint8_t percentPOT;
		percentPOT = potencia;
		percentPOT = 100 / potencia; // distrubuye la potencia uniformemente como dice angel

		percentPOT *= 10; // tiempo de encendido

		int i;
		for (i=1;i<potencia;i++)
		{
			encender(RESISTENCIA_SU);
			_delay_ms(8);
			apagar(RESISTENCIA_SU);
			_delay_ms(2);
			//_delay_ms(percentPOT);
		}

		//if (Output)

		//////////////////////////////////////

		char tempPIDASCII[7] = "";
		char tempMediaASCII[7] = "";

		itoa(Output,tempPIDASCII,10);

		itoa(AVGtemp,tempMediaASCII,10);

		lcd_gotoxy(0, 2);
		lcd_puts("AVG:");
		lcd_puts(tempMediaASCII);
		lcd_putc(0xdf);
		lcd_puts("C");

		lcd_gotoxy(0, 3);
		lcd_puts("PID:");
		lcd_puts(tempPIDASCII);
		lcd_putc(0xdf);
		lcd_puts("C");

		//////////////////////////////////////









		//uint16_t temp3;
		//temp3 = TablaTemperaturas[0];

		//uint8_t bandera = 0;
		//char banderaASCII[1] = "";

		//uint16_t media;
		//char tempMediaASCII[7] = "";

		/*if (temp3>=270)
		{
			while(1)
			{
				media = tempMedia(&TablaTemperaturas);
				itoa(media,tempMediaASCII,10);

				lcd_gotoxy(0, 3);
				lcd_puts("AVG:");
				lcd_puts(tempMediaASCII);
				lcd_putc(0xdf);
				lcd_puts("C");

				bandera = 10;

				apagar(RESISTENCIA_RE);
				apagar(RESISTENCIA_SU);
				apagar(VENTILADOR_RE);
				encender(VENTILADOR_INF);

				//! TODO si temp >=260 apagar y soplar por abajo
				//lcd_clrscr();
				lcd_gotoxy(0, 2);
				lcd_puts("Vent Inferior.   ");
				leerTermopares(&TablaTemperaturas);
				mensajeSerial(&TablaTemperaturas);
				mensajeLCD(&TablaTemperaturas);

				_delay_ms(750);
			}
		}
		else
		{
			if (temp3<=150)
			{
				encender(RESISTENCIA_RE);
				encender(RESISTENCIA_SU);
				//lcd_clrscr();
				lcd_gotoxy(0, 2);
				lcd_puts("RESISTENCIAS  ");
				itoa(bandera,banderaASCII,10);
				lcd_puts(banderaASCII);
			}
			else
			{
				encender(RESISTENCIA_RE);
				encender(RESISTENCIA_SU);
				encender(VENTILADOR_RE);
				//lcd_clrscr();
				lcd_gotoxy(0, 2);
				lcd_puts("RES + REFLOW  ");
				itoa(bandera,banderaASCII,10);
				lcd_puts(banderaASCII);
			}
		}*/
		//_delay_ms(750);
		//mantenerInterior(&TablaTemperaturas[3],150);

		//soplar(7);
	}
}

uint16_t tempMedia(uint16_t tablaTemp[3])
{
	uint8_t i;
	uint16_t media = 0;
	for (i=0;i<4;i++)
	{
		switch (i)
		{
		case 0: // tipo K sobre la tarjeta
			media = media + ((tablaTemp[i])/2);
			break;
		case 1: // tipo K debajo de la bandeja
			media = media + (tablaTemp[i]/6);
			break;
		case 2: // tipo K puerta
			media = media + (tablaTemp[i]/6);
			break;
		case 3: // tipo K camara de refusion
			media = media + (tablaTemp[i]/6);
			break;
		}
	}
	return media;
}

void mensajeSerial(int16_t tempCelsius[3])
{
	uint8_t j=0;

	char tempCelsiusASCII[7] = "";

	for (j=0;j<4;j++)
	{
		itoa(tempCelsius[j],tempCelsiusASCII,10);

		if (tempCelsius[j]==0xffff)
		{
			USART_Send_string("oo");
			USART_Send_string(",");
		}
		else
		{
			USART_Send_string(tempCelsiusASCII);

			USART_Send_string(",");
		}
		//USART_Send_string(mensaje0);
	}
	USART_Send_string("\n");
}

void leerTermopares(uint16_t tablaTemperaturas[3])
{
	uint8_t i=0;
	uint8_t hByte;
	uint8_t lByte;
	uint16_t tempCelsius;

	for (i=0;i<4;i++)
	{
		termocouple_read(i, &hByte, &lByte);
		tempCelsius = hByte;
		tempCelsius <<= 8;
		tempCelsius |= lByte;
		tempCelsius >>=5;
		if (termopar_abierto(&lByte))
		{
			tempCelsius = 0xffff;
		}
		tablaTemperaturas[i] = tempCelsius;
	}
}

void mantenerInterior(uint16_t *tempMedia,uint16_t tempDeseada)
{
	if (*tempMedia < tempDeseada)
	{
		encender(RESISTENCIA_RE);
		encender(RESISTENCIA_SU);
		lcd_gotoxy(0, 2);
		lcd_puts("Calentando...");
	}
	else
	{
		apagar(RESISTENCIA_RE);
		apagar(RESISTENCIA_SU);
		apagar(VENTILADOR_RE);
		encender(VENTILADOR_INF);
		lcd_gotoxy(0, 2);
		lcd_puts("Enfriando...");
	}
}

void mensajeLCD(int16_t tempCelsius[3])
{
	uint8_t i;
	for (i=0;i<4;i++)
	{
		char numeroDeSonda[4] = "";
		char mensajeLCD[7] = "";
		char tempCelsiusASCII[7] = "";

		itoa(tempCelsius[i],tempCelsiusASCII,10);

		strcat(mensajeLCD,"T");
		itoa(i,numeroDeSonda,10);
		strcat(mensajeLCD,numeroDeSonda);
		strcat(mensajeLCD,":");
		if (tempCelsius[i]==0xffff)
		{
			strcat(mensajeLCD,"ERR");
		}
		else
		{
			strcat(mensajeLCD,tempCelsiusASCII);
		}
		switch(i)
		{
			case 0:
				lcd_gotoxy(0, 0);
				lcd_puts(mensajeLCD);
				lcd_putc(0xdf);
				lcd_puts("C");
			break;
			case 1:
				lcd_gotoxy(0, 1);
				lcd_puts(mensajeLCD);
				lcd_putc(0xdf);
				lcd_puts("C");
			break;
			case 2:
				lcd_gotoxy(10, 0);
				lcd_puts(mensajeLCD);
				lcd_putc(0xdf);
				lcd_puts("C");
			break;
			case 3:
				lcd_gotoxy(10, 1);
				lcd_puts(mensajeLCD);
				lcd_putc(0xdf);
				lcd_puts("C");
			break;
		}
	}
}
