

	// SSR - PINOUT
	//		P4	->	RHOT0		<->	ATmega[12]	<->	PORTD:6	<-> PD6	<->	PIND0	<-	1
	//		P3 	->	MOT0		<->	ATmega[11]	<->	PORTD:5	<->	PD5	<->	PIND5 	<-	1
	//		P2	->	MOT1		<->	ATmega[5]	<->	PORTD:3	<->	PD3	<->	PIND3	<-	1
	//		P1	->	RHOT1		<->	ATmega[15]	<->	PORTB:1	<->	PB1	<->	PINB1	<-	1

#ifndef _ssr_h__
#define _ssr_h__

#include <avr/io.h>
#include <util/delay.h>

#define RESISTENCIA_REFLUJO		PIND6 //RHOT0
#define VENTILADOR_REFLUJO		PIND3 //MOT1
#define RESISTENCIA_SUPERIOR	PINB1 //RHOT1
#define VENTILADOR_INFERIOR 	PIND5 //MOT0

void todosArriba();
void todosAbajo();
void setup_ssr();

void soplar (uint8_t rendimiento);

void encender(uint8_t ch);
void apagar(uint8_t ch);

#endif
