

	// SPI
	//		MISO	<->	ATmega[23]	<->	PORTB:4	<->	PB4	<->	PINB4 	<-	1
	//		SCK		<->	ATmega[24]	<->	PORTB:5	<->	PB5	<->	PINB5	<-	1
	//		t0		<->	ATmega[14]	<->	PORTB:0	<-> PB0	<->	PINB0	<-	1
	//		t1		<->	ATmega[23]	<->	PORTC:0	<->	PC0	<->	PINC0 	<-	1
	//		t2		<->	ATmega[24]	<->	PORTC:1	<->	PC1	<->	PINC1	<-	1
	//		t3		<->	ATmega[17]	<->	PORTC:2	<->	PC2	<->	PINC2	<-	1

/*
 * Copyright (c) 2009 Andrew Smallbone <andrew@rocketnumbernine.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef _spi_h__
#define _spi_h__

#include <avr/io.h>

#define SPI_SS_PIN PORTB2
#define SPI_SCK_PIN PORTB5
#define SPI_MOSI_PIN PORTB3
#define SPI_MISO_PIN PORTB4


// SPI clock modes
#define SPI_MODE_0 0x00 /* Sample (Rising) Setup (Falling) CPOL=0, CPHA=0 */
#define SPI_MODE_1 0x01 /* Setup (Rising) Sample (Falling) CPOL=0, CPHA=1 */
#define SPI_MODE_2 0x02 /* Sample (Falling) Setup (Rising) CPOL=1, CPHA=0 */
#define SPI_MODE_3 0x03 /* Setup (Falling) Sample (Rising) CPOL=1, CPHA=1 */

// data direction
#define SPI_LSB 1 /* send least significant bit (bit 0) first */
#define SPI_MSB 0 /* send most significant bit (bit 7) first */

// whether to raise interrupt when data received (SPIF bit received)
#define SPI_NO_INTERRUPT 0
#define SPI_INTERRUPT 1

// slave or master with clock diviser
#define SPI_SLAVE 0xF0
#define SPI_MSTR_CLK4 0x00 /* chip clock/4 */
#define SPI_MSTR_CLK16 0x01 /* chip clock/16 */
#define SPI_MSTR_CLK64 0x02 /* chip clock/64 */
#define SPI_MSTR_CLK128 0x03 /* chip clock/128 */
#define SPI_MSTR_CLK2 0x04 /* chip clock/2 */
#define SPI_MSTR_CLK8 0x05 /* chip clock/8 */
#define SPI_MSTR_CLK32 0x06 /* chip clock/32 */


// setup spi
void setup_spi(uint8_t mode,   // timing mode SPI_MODE[0-4]
               int dord,             // data direction SPI_LSB|SPI_MSB
               int interrupt,        // whether to raise interrupt on recieve
               uint8_t clock); // clock diviser

// disable spi
void disable_spi(void);

// send and receive a byte of data (master mode)
uint8_t send_spi(uint8_t out);

// receive the byte of data waiting on the SPI buffer and
// set the next byte to transfer - for use in slave mode
// when interrupts are enabled.
uint8_t received_from_spi(uint8_t out);


void termocouple_read(uint8_t CS, uint8_t *h_Byte, uint8_t *l_Byte);
uint8_t termopar_abierto(uint8_t *l_Byte);
void hex2temp(uint8_t h_Byte, uint8_t l_Byte, uint8_t *tempEntera,uint8_t *tempDecimal);

#endif
