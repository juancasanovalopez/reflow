#ifndef F_CPU
#define F_CPU 16000000UL // 16 MHz clock speed
#endif

#include <avr/io.h>
#include <stdio.h>

#define BAUD 9600	  // Baud Rate
#define MYUBRR (((((F_CPU * 10) / (16L * BAUD)) + 5) / 10) - 1)

void setup_USART( unsigned int ubrr);
void USART_Sendbyte( unsigned char data );
void USART_Send_string(const char *str);
void USART_Send_int(unsigned int d);
void USART_Send_hex(uint8_t h);
unsigned char USART_Receive( void );

// Declaraciones de E/S simples    %%%%%%%%%%%%%% escribir en ssr.h
	//1.- Relacionar pines de microntrolador con sentencias en C
	//	Outputs
	//		p1	<->	ATmega[15]	<->	PORTB:1	<-> PB1	<->	PINB1	<-	1
	//		p2	<->	ATmega[16]	<->	PORTB:2	<->	PB2	<->	PINB2 	<-	1
	//		p3	<->	ATmega[11]	<->	PORTD:5	<->	PD5	<->	PIND5	<-	1
	//		p4	<->	ATmega[12]	<->	PORTD:6	<->	PD6	<->	PIND6	<-	1


